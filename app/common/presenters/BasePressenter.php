<?php

namespace App\Presenters;

use Nette;


abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var \WebLoader\Nette\LoaderFactory @inject */
    public $webLoader;

    /** @var array volitelne js k nacteni */
    public $customJs = [];

    /** @var array volitelne css k nacteni */
    public $customCss = [];

    /** @var array volitelne externi js k nacteni - URl */
    public $customRemoteJs = [];

    /** @var array volitelne externi css k nacteni - URl */
    public $customRemoteCss = [];

    /** @return CssLoader */
    protected function createComponentCss()
    {
        $webloader = $this->webLoader->createCssLoader('default');

        if ($this->customRemoteCss) {
            $webloader->getCompiler()->getFileCollection()->addRemoteFiles($this->customRemoteCss);
        }
        if ($this->customCss) {
            $webloader->getCompiler()->getFileCollection()->addFiles($this->customCss);
        }
        return $webloader;
    }

    /** @return JavaScriptLoader */
    protected function createComponentJs()
    {
        $webloader = $this->webLoader->createJavaScriptLoader('default');
        if ($this->customRemoteJs) {
            $webloader->getCompiler()->getFileCollection()->addRemoteFiles($this->customRemoteJs);
        }
        if ($this->customJs) {
            $webloader->getCompiler()->getFileCollection()->addFiles($this->customJs);
        }
        return $webloader;
    }
}
