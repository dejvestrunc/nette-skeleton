<?php

namespace App\Presenters;

use Nette;


class HomepagePresenter extends BasePresenter
{
    public function renderDefault()
    {
        // ukázka použití, může se smazat
        $this->customCss = [
            'slide.css'
        ];
    }
}
